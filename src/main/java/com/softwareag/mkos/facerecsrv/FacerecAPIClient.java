package com.softwareag.mkos.facerecsrv;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.URI;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.file.Path;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.PathResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

/**
 * Sends an image file to the facerec REST API and retrieves the result of the recognition.
 * 
 * @author MKOS
 *
 */
public class FacerecAPIClient {
	private String facerecEndpoint;

	private ExecutorService executor = Executors.newSingleThreadExecutor();

	public FacerecAPIClient(String facerecEndpoint) {
		super();
		this.facerecEndpoint = facerecEndpoint;
	}

	public Future<RecognitionResult> retrieveRecognitionResultAsync(Path filePath) {
		return executor.submit(() -> {
			return retrieveRecognitionResult(filePath);
		});
	}

	public RecognitionResult retrieveRecognitionResult(Path filePath) throws IOException {
		System.out.println("----------------RECOGNISING -------------" + filePath);
				
		RestTemplate rt = new RestTemplate();

		PathResource pathResource = new PathResource(filePath);
		LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<>();

		System.out.println("pathResource exists: " + pathResource.exists());

		map.add("file", pathResource);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);

		HttpEntity<LinkedMultiValueMap<String, Object>> requestEntity = new HttpEntity<LinkedMultiValueMap<String, Object>>(
				map, headers);
		String result = rt.postForObject(facerecEndpoint, requestEntity, String.class);

		RecognitionResult recognitionResult = new RecognitionResult(filePath, result);
		
		System.out.println("Recognised: " + result);

		return recognitionResult;
	}
}
