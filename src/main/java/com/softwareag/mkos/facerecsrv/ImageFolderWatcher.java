package com.softwareag.mkos.facerecsrv;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.ArrayList;
import java.util.Date;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.PathResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.HtmlUtils;

/**
 * Watches a specified folder and when a new file is created there, moves the file to static/images/.
 * 
 * Additionally, when over MAX_STORED_CAMERA_CAPTURES files have been moved, when
 * a new one is added, the oldest one is deleted.
 * @author MKOS
 *
 */
@Component
public class ImageFolderWatcher {
	
	
	private static final Logger log = LoggerFactory.getLogger(ImageFolderWatcher.class);


	private static final int MAX_STORED_CAMERA_CAPTURES = 15;
	
	private volatile Deque<Path> storedFiles;
	
	//@Autowired
	//private SocketHandler socketHandler;
	private File capturedImagesFolder;
	
	@Value("${imageFeed.watchFolder}")
	private String folderToWatchPathString;
	
	@Value("${imageFeed.targetFolder}")
	private String capturedImagesFolderPathString;
	
	@Value("${imageFeed.watchFolderCopyDelayMillis}")
	private int watchFolderCopyDelayMillis;
	
	public ImageFolderWatcher() {
		storedFiles = new LinkedList<Path>();
	}
	
	public ImageFolderWatcher(String folderToWatchPathString) {
		super();
		this.folderToWatchPathString = folderToWatchPathString;
		storedFiles = new LinkedList<Path>();
	}

	@EventListener(ApplicationReadyEvent.class)
	public void onApplicationReady() throws InterruptedException, IOException {
		beginWatchingFolder();
	}
	
	/**
	 * Thread safe
	 * @return
	 */
	public Path getLatestImagePath() {
		Path last;
		synchronized (storedFiles) {
			if (storedFiles.isEmpty()) {
				last = null;
			} else {
				last = storedFiles.getLast();
			}
		}
		return last;
	}

	public void beginWatchingFolder() throws InterruptedException, IOException {
		log.info(String.format("Watching folder %s. New files will be moved to %s. After %d files are moved, older files will be deleted",
				folderToWatchPathString,
				capturedImagesFolderPathString,
				MAX_STORED_CAMERA_CAPTURES));
		capturedImagesFolder = new ClassPathResource(capturedImagesFolderPathString).getFile();

		WatchService watcher = FileSystems.getDefault().newWatchService();		
		
		Path dir = Paths.get(folderToWatchPathString);
		//System.out.println("absolutePath exists" + dir.toFile().exists());

		WatchKey key = null;
		try {
			key = dir.register(watcher, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE,
					StandardWatchEventKinds.ENTRY_MODIFY);
		} catch (IOException x) {
			log.error("couldn't register watcher");
		}

		while (true) {
			List<WatchEvent<?>> events = key.pollEvents();
			if (events != null 
					&& !events.isEmpty()) {
				for (WatchEvent<?> watchEvent : events) {
					if (watchEvent.kind() == StandardWatchEventKinds.ENTRY_CREATE) {
						Path relPath = (Path) watchEvent.context();
						Path filePath = dir.resolve(relPath);
						log.trace("file created " + filePath.toFile());						
						
						// move from the pi folder to our http server static files folder
						//Thread.sleep(watchFolderCopyDelayMillis + 300); // for some reason the file won't move if we try immediately
						
						File destFile = capturedImagesFolder.toPath().resolve(relPath).toFile();
						
						FileUtil.copyFileUsingRandomAccessFile(filePath.toFile(), destFile);
						//Files.copy(filePath, destFile.toPath());
					
						//boolean moved = filePath.toFile().renameTo(destFile);
						log.debug(String.format("file copied to %s : %s", destFile, destFile.exists()));	
						
						// if there are over MAX_STORED_CAMERA_CAPTURES images,
						// delete the earliest one
						synchronized (storedFiles) {
							storedFiles.add(destFile.toPath());
							if (storedFiles.size() > MAX_STORED_CAMERA_CAPTURES) {
								Path removed = storedFiles.remove();
								log.trace("deleting " + removed);
								boolean deleted = removed.toFile().delete();
								log.trace("deleted: " + deleted);
							}
						}
					}
				}
			}
		}
	}
	
	

}