package com.softwareag.mkos.facerecsrv;

import java.nio.file.Path;

public class RecognitionResult {

	private Path filePath;
	private String result;

	public RecognitionResult(Path filePath, String result) {
		this.filePath = filePath;
		// TODO Auto-generated constructor stub
		this.result = result;
		this.result = this.result.replace("\"","");
		
	}

	public Path getFilePath() {
		return filePath;
	}

	public void setFilePath(Path filePath) {
		this.filePath = filePath;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	
}
