package com.softwareag.mkos.facerecsrv;

import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class FacerecService {
	
	
	private static final Logger log = LoggerFactory.getLogger(FacerecService.class);

	@Value("${facerec.endpoint}")
	private String facerecEndpoint;


	@Autowired
	private ImageFolderWatcher imageFolderWatcher;

	private FacerecAPIClient faceRecognitionRetriever;

	private volatile RecognitionResult lastResult;

	/**
	 * Continuously sends the newest image in the watched folder for face recognition and
	 * stores the result. Does not track the watch folder while waiting on a result, i.e.
	 * images might be skipped.  
	 */
	public FacerecService() {
		
	}
	
	public RecognitionResult getLastResult() {
		return lastResult;
	}

	@EventListener(ApplicationReadyEvent.class)
	public void onApplicationReady() throws InterruptedException, IOException {
		new Thread(() -> {
			run();
		}).start();
	}
	
	private void run() {
		log.info("Staring recognition service with ednpoint " + facerecEndpoint);
		faceRecognitionRetriever = new FacerecAPIClient(facerecEndpoint);
		Future<RecognitionResult> facerecResultFuture = null;
		Path latestImagePath = null;
		while (true) {
			Path imagePath = imageFolderWatcher.getLatestImagePath();
		
			if (imagePath == null 
					|| imagePath == latestImagePath) {
				continue;
			}
			
			latestImagePath = imagePath;
			
			if (facerecResultFuture == null) {
				facerecResultFuture = faceRecognitionRetriever.retrieveRecognitionResultAsync(imagePath);
			}
			
			try {
				log.info("Waiting on facerec result for " + imagePath);
				lastResult = facerecResultFuture.get();
				log.info("Facerec result: " +lastResult);
				
			} catch (InterruptedException | ExecutionException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} finally {
				facerecResultFuture = null;
			}
			
		}
	}

}
