package com.softwareag.mkos.facerecsrv;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {
	
	@Autowired
	SocketHandler socketHandler;
	
	public WebSocketConfig() {
		// TODO Auto-generated constructor stub
	}


	public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
		//SocketHandler socketHandler = new SocketHandler();
		registry.addHandler(socketHandler, "/name").setAllowedOrigins("*");
	}

}