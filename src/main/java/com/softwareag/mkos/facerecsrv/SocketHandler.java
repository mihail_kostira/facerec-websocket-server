package com.softwareag.mkos.facerecsrv;

import com.google.gson.Gson;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * The Websocket server handler. When a client connects, sends the latest image and the latest
 * facerec result.
 * @author MKOS
 *
 */
@Component
public class SocketHandler extends TextWebSocketHandler {

	private static final Logger log = LoggerFactory.getLogger(SocketHandler.class);

	
	@Autowired
	private ImageFolderWatcher imageFolderWatcher;
	
	@Autowired
	private FacerecService facerecService;
	
	List<WebSocketSession> sessions = new CopyOnWriteArrayList<>();

	@Value("${imageFeed.updateInterval}")
	private long imageFeedUpdateInterval;


	public SocketHandler() {

	}

	

	@Override
	public void handleTextMessage(WebSocketSession session, TextMessage message)
			throws InterruptedException, IOException {
		Map<String, String> value = new Gson().fromJson(message.getPayload(), Map.class);
		/*
		 * for(WebSocketSession webSocketSession : sessions) {
		 * webSocketSession.sendMessage(new TextMessage("Hello " + value.get("name") +
		 * " !")); }
		 */
		session.sendMessage(new TextMessage("Hello@ " + value.get("name") + " !"));
	}

	@Override
	public void afterConnectionEstablished(WebSocketSession session) throws Exception {
		// the messages will be broadcasted to all users.

		log.info("New socket client connected");
		sessions.add(session);

		new Thread(() -> {
			run(session);

		}).start();

	}

	private void run(WebSocketSession session) {
		// TODO Auto-generated method stub
		RecognitionResult lastResult = null;
		String recognitionResultString = "";
		File imageFile = null;
		while (true) {

			if (!session.isOpen()) {
				log.info("removing session");
				sessions.remove(session);
				break;
			}
						
			if (imageFolderWatcher.getLatestImagePath() == null) {
				continue;
			}
			
			imageFile = imageFolderWatcher.getLatestImagePath().toFile();
			
			recognitionResultString = "";
			lastResult = facerecService.getLastResult();
			if (lastResult != null) {
				recognitionResultString = lastResult.getResult();
			}
			
			
			log.debug("cam capture to be sent over socker: " + imageFile);

			// wait - sending the file immediately doesn't work(?)
			try {
				Thread.sleep(100);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			
			sendUpdate(session, imageFile, recognitionResultString);
			
			try {
				Thread.sleep(imageFeedUpdateInterval);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	private void sendUpdate(WebSocketSession session, File imageFile, String recognitionResultString) {
		log.info("Sending image update");
		String json = null;
		try {
			json = String.format("{ \"imagePath\": \"%s \", \n \"name\":\"%s\"  }", 
					"/images/" + imageFile.getName(),
					recognitionResultString);
			session.sendMessage(new TextMessage(json));
		} catch (Exception e) {
			System.out.println("error sending socket msg " + e);
			e.printStackTrace();
		}
	}

}